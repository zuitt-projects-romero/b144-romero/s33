const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		return: [true, 'First Name is Required']
	},
	lastName: {
		type: String,
		return: [true, 'Last Name is Required']
	},
	age: {
		type: String,
		return: [true, 'Age is Required']
	},
	gender: {
		type: String,
		return: [true, 'Gender is Required']
	},
	email: {
		type: String,
		return: [true, 'Email is Required']
	},
	password: {
		type: String,
		return: [true, 'Password is Required']
	},
	mobileNo: {
		type: String,
		return: [true, 'Mobile Number is Required']
	},
	isAdmin: {
		type: Boolean,
		return: false
	},
	// Enrollment field will be an arrar of objects
	enrollments:[ 
		{
			courseId: {
				type: String,
				required: [true, 'Course ID is required']
			},
			enrolledOn:{
				type: Date,
				default: new Date()
			},
			status:{
				type: String,
				default: "Enrolled"
			}

		}
	]
})

module.exports = mongoose.model('User', userSchema)