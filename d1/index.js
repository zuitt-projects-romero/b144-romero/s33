//index.js
// Set up the dependencies
const express = require('express');
const mongoose = require('mongoose');
// Allows us to control app's Cross origin Resource Sharing settings
const cors = require('cors')
// Route
const userRoutes = require('./routes/user')
const courseRoutes = require('./routes/course')
// Server setup
const app = express();
const port = 4000;
// Allows all resources/origing to access our backend application
// Enable all CORS
app.use(cors())




app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Defines the '/api/users' string tp be included for all routes defined in the 'user' route file
app.use('/api/users', userRoutes)
app.use('/api/courses', courseRoutes)


mongoose.connect("mongodb+srv://brands27:nevermore27@cluster0.eteec.mongodb.net/batch144_booking_system?retryWrites=true&w=majority", {
	useNewUrlparser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"))
db.once("open",() => console.log("We're connected to the cloud database"))


app.listen(port,()=>console.log(`Now listens to port ${port}`))   